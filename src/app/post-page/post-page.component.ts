import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../admin/shared/interfaces';
import { PostService } from '../shared/post.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss']
})
export class PostPageComponent implements OnInit {

  post$: Observable<Post>

  constructor(
    private postService: PostService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.post$ = this.route.params
    .pipe(switchMap((params: Params)=>{
      return this.postService.getById(params['id'])
    }))
  }

}
